package dayo2;

public class Demo6 {

	public static void main(String[] args) {
		String s = "25";
		byte b = Byte.parseByte(s);
		short sh = Short.parseShort(s);
		int i = Integer.parseInt(s);
		long l = Long.parseLong(s);
		//lower to higher
		System.out.println("String s : "+s);
		System.out.println("byte b : "+b);
		System.out.println("short sh : " +sh);
		System.out.println("int i : "+i);
		System.out.println("long l : "+l);
		System.out.println();
		
		l = 450000000;
		i = (int)l;
		sh = (short)i;
		b = (byte)sh;
		s = b+"";
		//higher to lower
		System.out.println("long l   : "+l);
		System.out.println("int i    : " +i);
		System.out.println("short sh : "+sh);
		System.out.println("byte b   : "+b);
		System.out.println("string s : "+s);
	}

}
