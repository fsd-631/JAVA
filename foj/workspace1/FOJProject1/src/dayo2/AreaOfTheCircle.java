package dayo2;

public class AreaOfTheCircle {
    public static double calculateArea(double radius) {
        return 3.14 * radius * radius;
    }
    
    public static void main(String[] args) {
        double radius = 5;
        double area = calculateArea(radius);
        System.out.println("Area of the circle with radius " + radius + " is: " + area);
    }
}
