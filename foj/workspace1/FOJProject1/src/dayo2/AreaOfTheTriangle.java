package dayo2;

public class AreaOfTheTriangle {
    public static double calculateArea(double base, double height) {
        return 0.5 * base * height;
    }
    
    public static void main(String[] args) {
        double base = 6;
        double height = 8;
        double area = calculateArea(base, height);
        System.out.println("Area of the triangle with base " + base + " and height " + height + " is: " + area);
    }
}
