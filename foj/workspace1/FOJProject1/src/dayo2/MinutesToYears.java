package dayo2;

public class MinutesToYears {
	public static void minutesToYears(long minutes) {
		long minutesInYear = 60 * 24 * 365;
		long years = minutes / minutesInYear;
		long remainingMinutes = minutes % minutesInYear;
		long days = remainingMinutes / (60 * 24);

		System.out.println(minutes + " minutes is approximately " + years + " years and " + days + " days.");

	}

	public static void main(String[] args) {
		long minutes = 3271600;
		minutesToYears(minutes);
	}
}

