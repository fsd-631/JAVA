package dayo2;

public class Demo7 {

	public static void main(String[] args) {
		int num1 = 10;
		int num2 = 20;
		int temp;
		System.out.println("SWAPPING USING TEMP VARIABLE");
		System.out.println("num1="+num1+"\n"+"num2="+num2+"\n");
		temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("num1="+num1+"\n"+"num2="+num2+"\n");
		System.out.println("SWAPPING USING WITHOUT TEMP VARIABLE");
		
		System.out.println("num1="+num1+"\n"+"num2="+num2+"\n");
		num1 = num1+num2;
		num2 =  num1-num2;
		num1 = num1-num2;
		System.out.println("num1="+num1+"\n"+"num2="+num2+"\n");
	}

}
