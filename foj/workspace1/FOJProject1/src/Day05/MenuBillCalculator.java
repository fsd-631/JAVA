package Day05;

import java.util.Scanner;
public class MenuBillCalculator {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String[] menuItems = {"1. Mutton Biryani - 600.00", "2. Chicken Biryani - 635.00", "3. Fish Biryani - 999.00", "4. Juice - 123.00", "5. Exit"};
		double[] itemPrices = {600.00, 635.00, 999.00, 123.00};
		System.out.println("Menu:");
		for (String item : menuItems) {
			System.out.println(item);
		}

		double totalBill = 0;
		while (true) {
			System.out.print("Enter your choice (1-5): ");
			int choice = scanner.nextInt();

			if (choice >= 1 && choice <= 4) {
				totalBill += itemPrices[choice - 1]; 
				System.out.println("Added " + menuItems[choice - 1].split(" - ")[0] + " to your bill.");
			} else if (choice == 5) {
				break; 
			} else {
				System.out.println("Invalid choice. Please enter a number between 1 and 5.");
			}
		}

		System.out.println("Your total bill is: " + totalBill);
		
		scanner.close();
	}
}