package Day05;

public class Demo4 {
    public static int sum(int num){
    	if (num == 1)
    		return 1;
    	return num + sum(num-1);
    }
    
    public static int prod(int num){
    	if (num == 1)
    		return 1;
    	return num * prod(num-1);
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("sum of the numbers upto 5 : "+sum(5));
		System.out.println("sum of the numbers upto 10 : "+sum(10));
		System.out.println("product of the numbers upto 5 : "+prod(5));
		System.out.println("product of the numbers upto 10 : "+prod(10));
	}

}
