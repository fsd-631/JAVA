package Day05;

public class Demo10 {
    public static void star(){
    	for(int i = 1; i <= 5; i++){
			for(int j = 1; j<=5; j++){
				System.out.print("* ");
			}
			System.out.println();
		}
    }
    
    public static void starbox(){
    	for(int i =1; i <= 5; i++){
    		for(int j = 1; j <= 5; j++){
    			if(i == 1 || i == 5){
    				System.out.print("* ");
    			}else{
    				if(j == 1 || j == 5){
    					System.out.print("* ");
    				}else{
    					System.out.print("  ");
    				}
    			}
    		}
    		System.out.println();
    	}
    }
    
    public static void starinner(){
    	for(int i =1; i <= 5; i++){
    		for(int j = 1; j <= 5; j++){
    			if((i == 1 || i == 5)||j == 1 || j == 5){
    				System.out.print("# ");
    			}else{
    				System.out.print("* ");
    			}
    		}
    		System.out.println();
    	}
    }
    
    public static void num(){
    	for(int i = 1; i <= 5; i++){
			for(int j = 1; j<=5; j++){
				System.out.print(j+" ");
			}
			System.out.println();
		}
    }
    
    public static void num1(){
    	for(int i = 1; i <= 5; i++){
			for(int j = 1; j<=5; j++){
				System.out.print(i+" ");
			}
			System.out.println();
		}
    }
    
    public static void num2(){
    	for(int i = 1; i <= 5; i++){
			for(int j = 5; j>=1; j--){
				System.out.print(j+" ");
			}
			System.out.println();
		}
    }
    
    public static void alpha(){
    	for(int i = 65; i <= 69; i++){
			for(int j = 1; j<=5; j++){
				System.out.print(((char)i)+" ");
			}
			System.out.println();
		}
    }
    
    public static void numpyramid(){
    	for(int i = 1;i<=5; i++){
    		for(int j = 1; j <= i; j++){
    			System.out.print(j+" ");
    		}
    		System.out.println();
    	}
    }
    
    public static void numrevpyramid(){
    	for(int i = 5;i<=1; i++){
    		for(int j = 1; j <= i; j++){
    			System.out.print(j+" ");
    		}
    		System.out.println();
    	}
    }
    
	public static void main(String[] args) {
		star();
		System.out.println();
		starbox();
		System.out.println();
		starinner();
		System.out.println();
		num();
		System.out.println();
        num1();
        System.out.println();
        num2();
        System.out.println();
        numpyramid();
        System.out.println();
        numrevpyramid();
        System.out.println();
        alpha();
	}

}
