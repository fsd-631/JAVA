package Day05;

public class Demo7 {
	public static boolean prime(int num){
		int c = 0;
		
   	 for(int i = 1; i <= num;i++){
   		 if(num%i==0){
   			 c++;
   		 }
   	 }
   	 
   	 if(c == 2){
   		 return true;
   	 }
   	 return false;
    }
	
	public static void main(String[] args) {
		System.out.println(prime(258));
		System.out.println(prime(2));
		System.out.println(prime(7));
	}

}
