package Day03;

public class Demo5 {

	public static int findGreatest(int num1, int num2, int num3) {
		int greatest = num1;

		if (num2 > greatest) {
			greatest = num2;
		}

		if (num3 > greatest) {
			greatest = num3;
		}

		return greatest;
	}

	public static void main(String[] args) {
		System.out.println("Greatest of the three numbers "+findGreatest(25,25,22));
	}
}
