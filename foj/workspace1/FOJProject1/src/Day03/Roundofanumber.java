package Day03;

public class Roundofanumber {

	public static void round(double num){
		int rNum = (int) (num + 0.5);

		System.out.println("After rounding the number " + num + " is  "+rNum );
	}

	public static void main(String[] args) {
		round(25214.25414);

	}

}