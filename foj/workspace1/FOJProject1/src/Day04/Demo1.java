package Day04;

public class Demo1 {

	public static void main(String[] args) {
		int sub1 = 99;
		int sub2 = 79;
		int sub3 = 40;
		int total = sub1+sub2+sub3;
		double avg = total/3;

		System.out.println("Sub1: "+sub1);
		System.out.println("Sub2: "+sub2);
		System.out.println("Sub3: "+sub3);
		System.out.println("Total Marks: "+total);
		System.out.println("Average Marks: "+avg);

		if(sub1>39 && sub2>39 && sub3>39){
			
			if(avg >= 75){
				System.out.println("PASSED IN DISTINCTION DIVISION");
			}
			else if(avg >= 60){
				System.out.println("PASSED IN FIRST DIVISION");
			}
			else if(avg >= 50){
				System.out.println("PASSED IN SECOND DIVISION");
			}
			else {
				System.out.println("PASSED IN THIRD DIVISION");
			}
			
		}else{
			System.out.println("FAILED!!!");
		}
	}

}
