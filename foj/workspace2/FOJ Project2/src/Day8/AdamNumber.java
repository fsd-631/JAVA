package Day8;

public class AdamNumber {

    public static int reverse(int n) {
        int rev = 0;
        while (n > 0) {
            int digit = n % 10;
            rev = rev * 10 + digit;
            n /= 10;
        }
        return rev;
    }


    public static boolean isAdam(int num) {
        int square = num * num;
        int revSquare = reverse(square);
        int revNum = reverse(num);
        int revNumSquare = revNum * revNum;
        return revSquare == revNumSquare;
    }

    public static void main(String[] args) {
        
        int number = 1;
        

        if (isAdam(number)) {
            System.out.println(number + " is an Adam number.");
        } else {
            System.out.println(number + " is not an Adam number.");
        }
    }
}
