package Day8;

public class Collatzsequence {
	public static void printCollatzSequence(int n) {
        System.out.print(n + " ");
        
        while (n != 1) {
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = 3 * n + 1;
            }
            System.out.print(n + " ");
        }
    }
	public static void main(String[] args) {
		printCollatzSequence(20);

	}

}
