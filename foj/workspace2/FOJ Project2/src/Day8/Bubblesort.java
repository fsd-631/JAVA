package Day8;

public class Bubblesort {
	
	public static int[] sortAscending(int arr[]){
		int temp = 0;
		
		for(int i=0;i<arr.length-1 ; i++){
			for(int j=0; j<arr.length-1-i; j++){
				if(arr[j] > arr[j+1]){
					temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		return arr;
	}
	public static void main(String[] args) {
		int arr[] = {10,50,21,36,69,8};
		System.out.println("Before Sorting");
		for(int i = 0; i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println("\n");
		arr = sortAscending(arr);
		System.out.println("After Sorting");
		for(int i = 0; i< arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}

}
