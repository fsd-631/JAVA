package Day8;

public class EvenOddsum {
	public static int calculateEvenSum(int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        return sum;
    }

    public static int calculateOddSum(int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                sum += i;
            }
        }
        return sum;
    }
	public static void main(String[] args) {
		int start = 1;
        int end = 25;
        
        int evenSum = calculateEvenSum(start, end);
        int oddSum = calculateOddSum(start, end);

        System.out.println("Sum of even numbers from " + start + " to " + end + " is: " + evenSum);
        System.out.println("Sum of odd numbers from " + start + " to " + end + " is: " + oddSum);

	}

}
