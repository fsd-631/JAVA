package Day8;

public class ArmStrong {
	public static int power(int num, int pow) {
        int result = 1;
        for (int i = 0; i < pow; i++) {
            result *= num;
        }
        return result;
    }

    public static int countDigits(int num) {
        int count = 0;
        while (num != 0) {
            num /= 10;
            count++;
        }
        return count;
    }

    public static boolean isArmstrong(int num) {
        int originalNum = num;
        int sum = 0;
        int numOfDigits = countDigits(num);
        
        while (num != 0) {
            int digit = num % 10;
            sum += power(digit, numOfDigits);
            num /= 10;
        }
        
        return sum == originalNum;
    }

    public static void main(String[] args) {
        
        int number = 153;
        

        if (isArmstrong(number)) {
            System.out.println(number + " is an Armstrong number.");
        } else {
            System.out.println(number + " is not an Armstrong number.");
        }
    }
}